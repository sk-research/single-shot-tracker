# Single Shot Tracker

A PyTorch 1.0+ implementation of [Deep Affinity Network for Multiple Object Tracking](https://arxiv.org/abs/1810.11780)
